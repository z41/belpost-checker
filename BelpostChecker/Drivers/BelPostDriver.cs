﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using BelpostChecker.Core;
using HtmlAgilityPack;

namespace BelpostChecker.Drivers
{
    public class BelPostDriver : ICheckDriver
    {

        private static Dictionary<string, PackageStatus> _packageStatuses = new Dictionary<string, PackageStatus>
        {
            {"MINSK PI 2", PackageStatus.customs},
            {"MINSK PI 3", PackageStatus.customs},
            {"пытка вручения", PackageStatus.localPost},
            {"Вручено", PackageStatus.delivered},

        };

        private static string searchUrl = "http://search.belpost.by/ajax/search";

        public void UpdateStatus(ref Package package)
        {
            if (package != null)
            {
                try
                {
                    var query = string.Format("item={0}&internal=2", package.TrackingCode);
                    var client = new WebClient();
                    client.Headers.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.114 Safari/537.36");
                    client.Headers.Add("X-Requested-With", "XMLHttpRequest");
                    client.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                    var responeString = client.UploadString(searchUrl, query);
                    client.Dispose();

                    // little magic
                    var decoded = Encoding.UTF8.GetString(Encoding.GetEncoding("windows-1251").GetBytes(responeString));

                    var htmlDoc = new HtmlDocument();
                    htmlDoc.LoadHtml(decoded);
                    var rows = htmlDoc.DocumentNode.Descendants().Where(desc => desc.Name.Equals("tr"));
                    if (rows.Any())
                    {
                        var lastRowText = rows.Last().InnerText;
                        var entry = _packageStatuses.FirstOrDefault(st => lastRowText.Contains(st.Key));
                        if (!entry.Equals(null))
                        {
                            package.Status = entry.Value;
                            package.StatusString = lastRowText;
                        }
                    }
                    else
                    {
                        package.Status = PackageStatus.unknown;
                    }
                }
                catch (Exception)
                {
                    
                }
                return;
            }
            throw new NotImplementedException();
        }



        public void Dispose()
        {

        }
    }
}
