﻿using System;

namespace BelpostChecker.Core
{
    public interface ICheckDriver : IDisposable
    {
        void UpdateStatus(ref Package package);
    }
}