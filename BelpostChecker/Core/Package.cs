﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using BelpostChecker.Annotations;

namespace BelpostChecker.Core
{
    public class Package 
    {
        public Package(string trackingCode, string description, DriverType driverType = DriverType.BelPost, DateTime? startToCheck = null, bool shouldBeChecked = true)
        {
            TrackingCode = trackingCode;
            Description = description;
            DriverType = driverType;
            StartToCheck = startToCheck ?? DateTime.Today.Date;
            ShouldBeChecked = shouldBeChecked;
        }

        public Package()
        {
            
        }

        private static JavaScriptSerializer _serializer;
        protected static JavaScriptSerializer Serializer
        {
            get { return _serializer ?? (_serializer = new JavaScriptSerializer()); }
        }

        public string Description { get; set; }
        public string TrackingCode { get; set; }
        public DriverType DriverType { get; set; }
        
        public DateTime? StartToCheck { get; set; }
        public bool ShouldBeChecked { get; set; }

        public DateTime? Updated { get; set; }
        public PackageStatus Status { get; set; }
        public string StatusString { get; set; }
        public string Link { get; set; }

        public string Serialize()
        {
            return Serializer.Serialize(new
            {
                TrackingCode = TrackingCode,
                Description = Description,
                DriverType = DriverType,
                StartToCheck = StartToCheck,
                ShouldBeChecked = ShouldBeChecked,
                Updated = Updated,
                Status = Status,
                StatusString = StatusString,
                Link = Link
            });
        }

        public static Package ParseJson(string json)
        {
            return Serializer.Deserialize(json, typeof (Package)) as Package;
        }
    }
}
