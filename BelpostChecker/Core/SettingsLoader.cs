﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BelpostChecker.Core
{
    public class SettingsLoader
    {
        protected string WorkDir;
        protected string PackageListPath;

        public SettingsLoader()
        {
            WorkDir = Directory.GetCurrentDirectory();
            PackageListPath = Path.Combine(WorkDir, "packages");
        }

        public IEnumerable<Package> LoadPackages()
        {
            if (!File.Exists(PackageListPath))
            {
                File.WriteAllText(PackageListPath, "");
            }
            return
                File.ReadAllLines(PackageListPath)
                    .Where(line => !string.IsNullOrWhiteSpace(line))
                    .Select(Package.ParseJson);
        }

        public void SavePackages(IEnumerable<Package> packages)
        {
            File.WriteAllLines(PackageListPath, packages.Select(package => package.Serialize()));
        } 
    }
}
