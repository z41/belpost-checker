﻿namespace BelpostChecker.Core
{

    public enum DriverType
    {
        BelPost
    }

    public enum PackageStatus
    {
        unknown,
        sent,
        customs,
        localPost,
        delivered
    }
}
