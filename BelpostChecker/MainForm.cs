﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BelpostChecker.Core;
using BelpostChecker.Drivers;

namespace BelpostChecker
{
    public partial class MainForm : Form
    {
        public SettingsLoader SettingLoader;
        public BindingList<Package> Packages;

        public MainForm()
        {
            InitializeComponent();
        }

        private void Check()
        {            // RC504154099CN
            using (var driver = new BelPostDriver())
            {
                Packages.Where(pack => pack.DriverType.Equals(DriverType.BelPost)).ForEach(pack => driver.UpdateStatus(ref pack));
            }
            dataGridView1.Refresh();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {

        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            SettingLoader = new SettingsLoader();
            Packages = new BindingList<Package>(SettingLoader.LoadPackages().ToList());
            dataGridView1.DataSource = Packages;

            dataGridView1.Columns["TrackingCode"].DisplayIndex = 0;
            dataGridView1.Columns["DriverType"].Visible = false;
            dataGridView1.Columns["ShouldBeChecked"].HeaderText = "Check";
            dataGridView1.Columns["ShouldBeChecked"].Width = 30;
            dataGridView1.Columns["Status"].Width = 60;

        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            SettingLoader.SavePackages(Packages);
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            Packages.Add(new Package("", ""));
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
            {
                dataGridView1.Rows.RemoveAt(row.Index);
            }
            foreach (DataGridViewCell cell in dataGridView1.SelectedCells)
            {
                dataGridView1.Rows.RemoveAt(cell.RowIndex);
            }
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            Check();
        }
    }
}
